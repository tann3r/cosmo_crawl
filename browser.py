# -*- coding: utf-8 -*-
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtWebKit import *

class Render(QWebPage):
    def __init__(self, url):
        self.app = QApplication(sys.argv)
        QWebPage.__init__(self)
        self.loadFinished.connect(self._loadFinished)
        self.mainFrame().load(QUrl(url))
        self.app.exec_()

    def _loadFinished(self, result):
        self.frame = self.mainFrame()
        self.app.quit()

def getHtml(str_url):
    r_html = Render(str_url)
    html = r_html.frame.toHtml()
    html = unicode(html)
    html = html.encode('utf-8')

    return html

# url = "http://en.koreadepart.com/item/1436851479/ossion-healing-lip-stick"
# html = getHtml(url)
# html = unicode(html)

# f = open('test.html', 'w')
# f.write(html.encode('utf-8'))
# f.close()




# from zombie import Browser
# b = Browser()
# b.visit(url)
# print b.body.text


# from ghost import Ghost
# ghost = Ghost()
# page, extra_resources = ghost.open(url)
# assert page.http_status==200 and 'jeanphix' in ghost.content

# import spynner

# browser = spynner.Browser()
# browser.load(url, timeout=60)
# # browser.select("#esen")
# # browser.fill("input[name=enit]", "hola")
# # browser.click("input[name=b]")
# browser.wait_page_load()
# print browser.url, len(browser.html)
# #browser.html
# f = open('test3.html', 'w')
# f.write(browser.html.encode('utf-8'))
# f.close()
# browser.close()
