import requests
from bs4 import BeautifulSoup


def get_pages_links(link, domain=None):
    links = []
    r = requests.get(link)
    domain = r.url
    print domain

    next = link
    while next:
        # print 'NEXT: ' + next
        r = requests.get(next)
        links.append(r.url)

        tree = BeautifulSoup(r.text)

        pages = tree.findAll('div', {'class': 'paginate'})[0]

        next = ''
        for page in pages.findAll('a'):
            if 'class' not in page.attrs:
                print domain + page['href'][2:]
                links.append(domain + page['href'][2:])

            elif 'next' in page.attrs['class']:
                #print '!#!#!@#!#@#!@#!#@#!@#!@#'
                next = domain + page['href'][2:]

    return links



url = 'http://en.koreadepart.com/cosmetics/'
domain = 'http://en.koreadepart.com'
links = {}

r = requests.get(url)

tree = BeautifulSoup(r.text)
tree = tree.findAll('div', {'class': 'categorytype'})[0]#[0].findNextSibling('ul')[0]

for child in tree:
    if child.name == 'div' and 'type-02' in child['class']:
        categories = child.findNextSibling('ul')


for link in categories.findAll('a'):
    links[link['title']] = domain + link['href']

del links['Cleansing (1,985)']

for product_type, link in links.iteritems():
    print "TYPE: " + product_type + ' LINK: ' + link
    print get_pages_links(link, domain)








#print tree


