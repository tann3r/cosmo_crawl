# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import pprint
import browser
import os
import re
from datetime import datetime

startTime = datetime.now()

#url = "http://en.koreadepart.com/item/1436851479/ossion-healing-lip-stick"
#url = "http://en.koreadepart.com/item/1437445421/skinfood-tea-tree-cleansing-foam"
url = "http://en.koreadepart.com/item/1436513688/espesso-plus-hair-color-treatment"

#r = requests.get(url)
html = browser.getHtml(url)
print 'FETCHED'
print datetime.now() - startTime
#print r.text
product = {}
#tree = BeautifulSoup(r.text)
tree = BeautifulSoup(html)
tree = tree.findAll('div', {'id': 'item-content'})[0]

product['title'] = tree.findAll('div', {'id': 'product-title'})[0].getText().strip()
product['keywords'] = tree.findAll('p', {'class': 'item-keyword'})[0].getText().strip()
product['price'] = ''
product['original_price'] = ''
product['weight'] = tree.findAll('input', {'name': 'it_rweight'})[0]['value']

product_colors = tree.findAll('select', {'name': 'it_opt7'})
if product_colors:
    product['color/option'] = []
    for child in product_colors[0].findChildren():
        if child['value'] != 'select':
            product['color/option'].append(child.getText())

product_review = tree.findAll('div', {'class': 'product-review'})[0]
for child in product_review.findChildren():
    if child.name == 'li':
        if 'code : ' in child.getText():
            product['barcode'] = child.getText().replace('code : ', '')
        # if 'weight : ' in child.getText():
        #     product['weight'] = child.getText().replace('weight : ', '')
        if 'Brand : ' in child.getText():
            product['brand'] = child.getText().replace('Brand : ', '')

product_explan = tree.findAll('div', {'id': 'div_explan'})[0]
product_explan_list = [s.strip() for s in product_explan.getText().encode('utf-8').splitlines() if s]
desc_res = []
ingredients = []
usage = []
volume = []
for index, line in enumerate(product_explan_list):
    if re.search(' *▶ *Product *Description *:?', line, re.I):
        for desc_line in product_explan_list[index+1:]:
            if '▶' not in desc_line:
                desc_line = desc_line.replace('\xc2\xa0', '')
                desc_res.append(desc_line)
            else:
                break

    if re.search(' *▶ *Main *ingredients *:?', line, re.I):
        for desc_line in product_explan_list[index+1:]:
            if '▶' not in desc_line:
                desc_line = desc_line.replace('\xc2\xa0', '')
                ingredients.append(desc_line)
            else:
                break

    if re.search(' *▶ *How *to *use *:?', line, re.I):
        for desc_line in product_explan_list[index+1:]:
            if '▶' not in desc_line:
                desc_line = desc_line.replace('\xc2', '')
                desc_line = desc_line.replace('\xa0', '')
                usage.append(desc_line)
            else:
                break

    match = re.search(' *▶ *Volume *:?', line, re.I) or re.search(' *▶ *Capacity *:?', line, re.I)
    if match:
        for desc_line in product_explan_list[index+1:]:
            if '▶' not in desc_line:
                desc_line = desc_line.replace('\xc2', '')
                desc_line = desc_line.replace('\xa0', '')
                volume.append(desc_line)
            else:
                if not filter(None, volume):
                    #print match.group(0)
                    volume.append(line.split(match.group(0))[1].strip())
                break

product['main_description'] = os.linesep.join(filter(None, desc_res))
product['ingredient'] = os.linesep.join(filter(None, ingredients))
product['how_to_use'] = os.linesep.join(filter(None, usage))
product['volume'] = os.linesep.join(filter(None, volume))
product['product_type'] = ''

product_info = tree.findAll('div', {'id': 'products-info'})[0]

for child in product_info.findChildren():
    if child.name == 'th' and child.getText().strip() == 'Country of Origin':
        product['country_of_manufacture'] = child.findNextSibling('td').getText().encode('utf-8').replace('\xa0', '').replace('\xc2', '')

    if child.name == 'th' and child.getText().strip() == 'Skin Type':
        product['skin_type'] = child.findNextSibling('td').getText().encode('utf-8').replace('\xa0', '').replace('\xc2', '')

    if child.name == 'th' and 'Effect' in child.getText().strip() and 'Efficacy' in child.getText().strip():
        product['effect_efficacy'] = child.findNextSibling('td').getText().encode('utf-8').replace('\xa0', '').replace('\xc2', '')

    if child.name == 'th' and child.getText().strip() == 'Instructions for use':
        product['instruction_for_use'] = child.findNextSibling('td').getText().encode('utf-8').replace('\xa0', '').replace('\xc2', '')

    #if child

#product['product_weight'] = ''

for child in tree.findAll('div', {'class': 'img-view'})[0].findChildren():
    if child.name == 'img':
        product['prod_img_1'] = child['src']

for child in product_explan.findAll('img'):
    product['prod_img_' + str(len([(key, value) for key, value in product.iteritems() if key.startswith('prod_img_')]) + 1)] = child['src']


print '##############'
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(product)
print '##############'
#print tree.select('div[id="product-title"]')

print datetime.now() - startTime

assert 'title' in product, "title"
assert 'keywords' in product, "keywords"
assert 'original_price' in product, "original_price"
assert 'price' in product, "price"
assert 'weight' in product, "weight"
assert 'volume' in product, "volume"
assert 'color/option' in product, "color/option"
assert 'barcode' in product, "barcode"
assert 'main_description' in product, "main_description"
assert 'country_of_manufacture' in product, "country_of_manufacture"
assert 'brand' in product, "brand"
assert 'ingredient' in product, "ingredient"
assert 'how_to_use' in product, "how_to_use"
assert 'product_type' in product, "product_type"
assert 'skin_type' in product, "skin_type"
assert 'effect_efficacy' in product, "effect_efficacy"
assert 'instruction_for_use' in product, "instruction_for_use"
assert 'prod_img_1' in product, "prod_img_1"
assert 'prod_img_2' in product, "prod_img_2"
assert 'prod_img_3' in product, "prod_img_3"
assert 'prod_img_4' in product, "prod_img_4"
assert 'prod_img_5' in product, "prod_img_5"



#print len(web_result_divs)
