import sys
from collections import deque # threadsafe datatype
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *
from PyQt4.QtNetwork import *
NUM_THREADS = 3 # how many threads to use

class Render(QWebView):
    active = deque() # track how many threads are still active
    data = {} # store the data

    def __init__(self, urls):
        QWebView.__init__(self)
        self.loadFinished.connect(self._loadFinished)
        self.webkit = QWebView()
        #self.webkit.page().userAgentForUrl = 'Mozilla/5.0 (X11; Linux; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Midori/0.4'
        self.urls = urls
        self.crawl()

    def crawl(self):
        try:
            url = self.urls.pop()
            #print 'downloading', url
            Render.active.append(1)
            #self.webkit.page().userAgentForUrl = 'Mozilla/5.0 (X11; Linux; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Midori/0.4'
            #print self
            #print dir(self.page())
            req = QNetworkRequest(QUrl(url))
            req.setRawHeader('User-Agent', '')
            #self.load(QUrl(url))
            self.load(req)
        except IndexError:
            # no more urls to process
            if not Render.active:
                # no more threads downloading
                #print 'finished'
                self.close()

    def _loadFinished(self, result):
        # process the downloaded html
        frame = self.page().mainFrame()
        url = str(frame.url().toString())
        Render.data[url] = frame.toHtml()
        Render.active.popleft()
        self.crawl() # crawl next URL in the list

app = QApplication(sys.argv) # can only instantiate this once so must move outside class
#urls = deque(['http://en.koreadepart.com/item/1436513688/espesso-plus-hair-color-treatment', 'http://webscraping.com/questions',
#    'http://webscraping.com/blog', 'http://webscraping.com/projects'])
urls = deque(['http://www.whatsmyua.com/'])
renders = [Render(urls) for i in range(NUM_THREADS)]


# req = QNetworkRequest(QUrl('http://www.whatsmyuseragent.com/'))
# req.setRawHeader('User-Agent', 'Mozilla/5.0 (X11; Linux; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Midori/0.4')
# view = QWebView()
# view.load(req)
#print dir(req)



app.exec_() # will execute qt loop until class calls close event
#print Render.data.keys()
#print unicode(Render.data['http://en.koreadepart.com/item/1436513688/espesso-plus-hair-color-treatment']).encode('utf-8')
print unicode(Render.data['http://www.whatsmyua.com/']).encode('utf-8')

#print renders[0].frame.toHtml()


