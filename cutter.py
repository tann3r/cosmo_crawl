# -*- coding: utf-8 -*-
import re
import os

desc = """


▶ Manufacturer: Korea cosmetics

▶ Origin: South Korea

▶ Capacity: 3.5g
▶ volume     :
3.5g


▶ Product Description

 

: Lipstick prevents loss of moisture, maintains the optimum level of moisture, keeps the brightness for a long time.

: Texture creates a smooth and soft cover comfortable, thanks to the formula Stimu-tex effectively restores and maintains skin elasticity lips.

: Pearl extract helps maintain the moisture level of the skin and prevents dryness, its crystalline structure reflects UV radiation. 
 
: The product uses the formula of joint development of the Korean and the French Institute, which specializes in the study of marine raw materials CODIF.


▶ How to use:

: Apply lipstick to dry, clean skin of the lips.

▶ Color options: 
"""

# desc = """
# ▶ Manufacturer : E&M

# ▶ Origin: South Korea

# ▶ Volume: 300ml

# ▶ Main ingredients

# : Diamond powder, gardenia flower, vegetable protein extract from grape skins


# ▶     Product Description :
# 1
# : Express Tint-pack for damaged hair, also provides coloring and hair dying within 2 minutes.

# : It has weak acid formula, keeps pH 4.5 balance. Easily rinses off the colored scalp.

# : Improves blood circulation, stimulates growth, strengthens hair roots and prevents hair loss. It provides a comprehensive healing effect on the hair and scalp.

# : The hair looks healthy, supple and well-groomed, easy fit, get rich gloss, hair color becomes deeper and more vivid.

# : Diamond powder allows restores rich, brilliant shine.

# : Does not contain ammonia odor.


# ▶ How to use:

# : Thoroughly rinse your hair and scalp with shampoo, do not use conditioner or any hair packs.

# : Dry hair with a towel. Wear rubber gloves and spread the mask over the entire length of hair.

# : Leave on for 2 minutes, then thoroughly rinse the scalp and hair with warm water.

# : Do not use shampoo and hot water! Dry hair with a dryer.

# * TIP.
# * It can be mixed with each other to produce different colors, tones and hues. It allows you to receive about 100 shades, having a base of 8 colors.
# * You can use the lock color.

# ▶ How to use:

# : Thoroughly rinse your hair and scalp with shampoo, do not use conditioner or any hair packs.

# : Dry hair with a towel. Wear rubber gloves and spread the mask over the entire length of hair.

# : Leave on for 2 minutes, then thoroughly rinse the scalp and hair with warm water.

# : Do not use shampoo and hot water! Dry hair with a dryer.

# * TIP.
# * It can be mixed with each other to produce different colors, tones and hues. It allows you to receive about 100 shades, having a base of 8 colors.
# * You can use the lock color.

# ▶ test:
# ▶ Color options:
# ▶ Color options:
# ▶ Color options:
# ▶ Color options:
# """
desc_lines = [s for s in desc.splitlines() if s]
desc_res = []
volume = []
for index, line in enumerate(desc_lines):
    search_part = re.search(' *▶ *Product *Description *:?', line)
    #print dir(search_part)
    if search_part:
        print search_part.string
    if re.search(' *▶ *Product *Description *:?', line):
        for desc_line in desc_lines[index+1:]:
            if '▶' not in desc_line:
                desc_res.append(desc_line)
            else:
                break

    match = re.search(' *▶ *Volume *:?', line, re.I) or re.search(' *▶ *Capacity *:?', line, re.I)
    if re.search(' *▶ *Volume *:?', line, re.I) or re.search(' *▶ *Capacity *:?', line, re.I):
        for desc_line in desc_lines[index+1:]:
            if '▶' not in desc_line:
                desc_line = desc_line.replace('\xc2', '')
                desc_line = desc_line.replace('\xa0', '')
                volume.append(desc_line)
            else:
                if not filter(None, volume):
                    #print match.group(0)
                    print line.split(match.group(0))[1].strip()
                break

print volume

#print os.linesep.join(desc_res)
# cutted = desc.split("▶ Product Description")[1]
# pos = cutted.find('▶')
# res = cutted[:563]

# print res.strip()

# res = re.search('▶ *Product Description *:?\n(.*\n)* *[^▶]( *.*)', desc, re.M)
# #res = re.search('▶ *Product Description *:?\n*((.*\n*)*)▶ How.*', desc, re.M)
# #res = re.findall('.*Product (.*) .*', desc, re.MULTILINE)
# print res
# print res.group(1)
